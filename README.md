

# Bootproject on React 

*This project contains an example and tests on how to create React JS dynamic components.*
See the result of the project on the [Project page](https://barefootapp.azurewebsites.net/) 


![Assets](assets/templates.png)



## Page templates and zones
Page class is the only route in this application. 
Instead of multiple routes it catches all routes 
and parses the Template and Zones for just that page.

A Page can have a predefined Template that contain 
Zones on where to put content(Components).

## Zone
A Zone can have multiple predefined regions, 
for eg. `<Zone zone="content" />` that will get 
all Components for just that zone and that particular Page.
