﻿import React, { Component } from "react";
import { TopicContent } from '../../api';


class Post extends Component {

    constructor(props) {
        super(props);
        
        this.state = {
            current: document.location.pathname.replace(TopicContent.topic.strategy, "")
        };
        this.createMarkup = this.createMarkup.bind(this);
    }

    createMarkup() {
        return { __html: TopicContent.topic.title };
    }

    render() {
        if (this.state.current !== undefined){
            const post = TopicContent.topics.filter(r => r.path === this.state.current)[0];
            if (post !== undefined) {
                return (
                    <div className="sectionroot">
                        <section>
                            <div className="sharedSection">
                                <div className="sectioncontent">
                                    <header><h2>ProjectsA</h2></header>
                                    <div>
                                        <h1>{post.title}</h1>
                                        <p>{post.content}</p>
                                    </div>

                                </div>
                            </div>
                        </section>
                    </div>
                );
            } else {
                return (<h1 dangerouslySetInnerHTML={ this.createMarkup() } />);
            }
        }
    }
}

export default Post;