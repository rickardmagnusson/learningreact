﻿import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { withRouter } from "react-router";
import Post from "./post";

import { TopicContent} from '../../api';

class Topics extends Component {

    render() {
        return (
            <div className="sectionroot">
                <section>
                    <div className="sharedSection">
                        <div className="sectioncontent">
                            <header><h2>{TopicContent.topic.title}</h2></header>
                            <div>
                                <ul>
                                    {
                                        TopicContent.topics.map(topic => <li key={topic.id}><Link className="p-3" to={`/topics/${topic.path}`} key={topic.id}>{topic.title}</Link></li>)
                                    }
                                </ul>
                            </div>
                           
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

export default withRouter(Topics);