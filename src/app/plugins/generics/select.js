﻿import React from "react";
import { Link, Route } from 'react-router-dom';


/**
 * Usage: <Select items={source} /> const source = {"data":[ {"id": "1", value: "Value"} ]}
 * @description 
 * @param {props} props The source from where to generate.
 * @returns {Array} Select list
 */
const Select = (props) => {
    return (
        <React.Fragment>
            {props.items.data.map(item => (
                <React.Fragment key={item.id}>
                    <option value={item.id} text={item.value}>{item.value}</option>
                </React.Fragment>
            ))}
        </React.Fragment>
    );
};
export default Select;



// eslint-disable-next-line
const CustomLink = ({ children, to, exact }) => (
    <Route path={to} exact={exact} children={({ match }) => (
        <div className={match ? 'active' : ''}>
            {match ? '> ' : ''}
            <Link to={to}>
                {children}
            </Link>
        </div>
    )} />
);



// eslint-disable-next-line
class Storage extends React.Component {

    constructor() {
        super();
        this.save = this.save.bind(this);
    }

    state = {
        localStorageAvailable: false
    };

    componentDidMount() {
        this.checkLocalStorageExists();
    }

    checkLocalStorageExists() {
        const testKey = 'test';

        try {
            localStorage.setItem(testKey, testKey);
            localStorage.removeItem(testKey);
            this.setState({ localStorageAvailable: true });
        } catch (e) {
            this.setState({ localStorageAvailable: false });
        }
    }

    load(key) {
        if (this.state.localStorageAvailable) {
            return localStorage.getItem(key);
        }

        return null;
    }

    save(key, data) {
        if (this.state.localStorageAvailable) {
            localStorage.setItem(key, data);
        }
    }

    remove(key) {
        if (this.state.localStorageAvailable) {
            localStorage.removeItem(key);
        }
    }

    render() {
        return (
            <React.Fragment>
                {this.props.render({ load: this.load, save: this.save, remove: this.remove })}
            </React.Fragment>
        );
    }
}