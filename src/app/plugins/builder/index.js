﻿import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Topics from "./topics";

export default class Builder extends Component {

    render()
    {
        return (

        <div className="sectionroot">
            <section>
                <div className="sharedSection">
                    <div className="sectioncontent">
                        <div>
                            <Router>
                                <div>
                                    <Route path="/projects" component={Topics} />
                                </div>
                            </Router>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        );
    }
}