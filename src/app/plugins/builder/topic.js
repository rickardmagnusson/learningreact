﻿import React, { Component } from "react";
import { TopicContent } from "../../api";



export default class Topic extends Component {

    render() {
           
        var post = TopicContent.topics.filter(r => r.path === document.location.pathname.replace(TopicContent.topic.strategy, ""))[0];
        if (post !== undefined) {
            var html = { __html: post.content };
            return (
                <div>
                    <h1>{post.title}</h1>
                    <span dangerouslySetInnerHTML={html} />
                </div>
            );
        }
    }
}