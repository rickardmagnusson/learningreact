﻿import React, { Component } from "react";
import {  Route, Link } from "react-router-dom";
import Topic from "./topic";
import { TopicContent } from "../../api";



import $ from "jquery";

$.fn.extend({
    timeline: function () {

        return this.each(function () {
            // Store content
            var $userContent = $(this).children('a');

            // Create required content divs
            $userContent.each(function () {
                $(this).addClass('timeline-content').wrap('<div class="timeline-point"><div class="timeline-block"></div></div>');
            });

            //Add icon
            $(this).find('.timeline-point').each(function () {
                $(this).prepend('<div class="timeline-icon"></div>');
            });
        });
    }
});


//eslint-disable-next-line
class Head extends Component {

    render() {
        
        if (document.location.pathname.split("/").length > 2) {
            return ("");
        } else {
            var post = TopicContent.topics[0];
            var html = { __html: post.content };
            return (
                <div>
                    <h1>{post.title}</h1>
                    <span dangerouslySetInnerHTML={html} />
                </div>
            );
        }
    }
}


//eslint-disable-next-line
export default class Topics extends Component {


    constructor() {
        super();
        this.currentClass = this.currentClass.bind(this);
    }

    componentDidMount() {
        $(".timeliner").timeline();
    }


    currentClass(path) {
        var url = document.location.pathname;
        var parts = url.split("/");
        var part = url + parts[0];
        if (url === part)
            return "nav-link active";

        return path === document.location.pathname ? "nav-link active" : "nav-link";
    }

    renderClick(path) {
        try {
            var ga = window.ga;
            ga('set', 'page', path);
            ga('send', 'pageview');
        } catch (e) {
            console.log("Error: " + path);
        }
    }


    render() {

        return (

            <div className="row">
                <div className="col-lg-9">
                    <Head />
                    <Route path={`${this.props.match.path}/:id`} key="lnk" component={Topic} data={this.props} />
                 </div>
                <div className="col-lg-3 sidebar timeliner"><h4>{TopicContent.topic.title}</h4>
                    {TopicContent.topics.map(topic => <Link key={topic.path} className={this.currentClass(topic.path)} onClick={() => this.renderClick(this.props.match.url +"/" + topic.path)} to={`${this.props.match.url}/${topic.path}`}>{topic.title}</Link>)}
                </div>
            </div>
        );
    }

}
