﻿import React from "react";
import PropTypes from "prop-types";
import { withRouter } from "react-router";

/** Component that shows the pathname of the current location */
class ShowLocation extends React.Component {
    static propTypes = {
        location: PropTypes.object.isRequired
    };

    render() {
        const { location} = this.props;
        return <div>Location: <span>Home: </span> {location.pathname}</div>;
    }
}

export default withRouter(ShowLocation);