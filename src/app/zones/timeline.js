﻿import React, { Component } from 'react';
import { TimelineContent } from "../api";
import $ from "jquery";



$.fn.extend({
    timeliner: function () {

        return this.each(function () {
            // Store content
            var $userContent = $(this).children('div');

            // Create required content divs
            $userContent.each(function () {
                $(this).addClass('timeline-content').wrap('<div class="timeline-point"><div class="timeline-block"></div></div>');
            });

            //Add icon
            $(this).find('.timeline-point').each(function () {
                $(this).prepend('<div class="timeline-icon"></div>');
            });
        });
    }
});


/** Contains a defintion and a list for boxes*/
export default class Timeline extends Component {


    componentDidMount() {
        $(".timeliner").timeliner();
    }


    render() {

        /** Fetch the dataitems from current page*/
        var data = TimelineContent.items.filter(p => p.path === document.location.pathname && p.zone === this.props.props.zone);
        var html = "";


        for (var i in data) {
            var item = data[i];

            /** Current content item*/
            var contents = "";
            var header = "";

            if (item.list.length > 0) {
                var list = item.list;
                contents += `<header><div><h2>` + item.title + `</h2><h4>`+item.content+`</h4></div></header>`;
                for (var a in list)
                    contents += "<div><h3>" + list[a].header + "</h2><p>" + list[a].content + "</p></div>";
            }

            var c = `<div class="sectionroot">
                <section>
                    <div class="sharedSection">
                        <div class="sectioncontent">
                            <div class="timeliner">
                           `+ header + `
                           `+ contents + `
                            </div>
                        </div>
                    </div>
                </section>
            </div>`;
            html += c;
        }
        var obj = { __html: html };
        return (<span dangerouslySetInnerHTML={obj} />);

    }
}