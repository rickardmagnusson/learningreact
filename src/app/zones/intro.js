﻿import React, { Component } from 'react';

export default class Intro extends Component {


    render() {

        return (
            <div className="sectionroot">
                <section>
                    <div className="sectioncontent">
                        <div className="sharedSection">
                            <h1><span>Boot</span><span>project </span><span>-developer</span><span>connection</span></h1>
                            <div className="imagecontainer">
                                <img alt="Logo back" src="img/logobg.svg" />
                            </div>
                            <div>
                                <p className="intro">Bootproject is development base for all projects and also where I create and test out new features from both .NET, javascript frameworks and more.<br /></p>
                                <a className="biglink" href="https://bitbucket.org/rickardmagnusson/learningreact/src/master/" rel="noopener noreferrer" target="_blank">Grab your copy on bitbucket</a>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}




