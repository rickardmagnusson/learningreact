﻿import React, { Component } from 'react';
import { ContentList } from "../api";


/** Contains a defintion and a list for boxes*/
export default class ContentItem extends Component {

    render() {

        /** Fetch the dataitems from current page*/
        var data = ContentList.items.filter(p => p.path === document.location.pathname && p.zone===this.props.props.zone);
        var html = "";


        for (var i in data)
        {
            var item = data[i];
 
            /** Current content item*/
            var contents = item.content;
            var header = "";

            if (item.list.length > 0) {
                var list = item.list;
                contents += `<header><img src=` + item.icon + ` alt="multi" class="mini" /> <h2>` + item.title + `</h2></header>`;
                contents += "<ul class=\" shared_grid\">";
                for (var a in list)
                    contents += "<li><h2><img src=" + list[a].icon + " alt=\"multi\" class=\"small\" /> " + list[a].header + "</h2><p>" + list[a].content + "</p></li>";
                contents += "</ul>";
            } else {
                header = `<header><img src=` + item.icon + ` alt="multi" class="mini" /> <h2>` + item.title + `</h2></header>`;
            }

            var c = `<div class="sectionroot` + item.style +`">
                <section>
                    <div class="sharedSection">
                        <div class="sectioncontent">
                           `+ header +`
                           `+ contents +`
                        </div>
                    </div>
                </section>
            </div>`;
            html += c;
            
            
        }
        var obj = { __html: html };
        return ( <span dangerouslySetInnerHTML={obj} /> );
        
    }
}