﻿import React, { Component } from 'react';
import { Data } from "../api";


/** Contains a defintion and a list for boxes*/
export default class ContentItem extends Component {

    //TODO: Need to apply a filter here...
    render() {

        /** Fetch the dataitem from current page*/
        var data = Data.filter(p => p.path === document.location.pathname)[0];


        return (
            <div className="sectionroot">
                <section>
                    <div className="sharedSection">
                        <div className="sectioncontent">
                            <header><img src={data.icon} alt="multi" className="mini" /> <h2>{data.title}</h2></header>
                            <ul className=" shared_grid">
                                {list.map(i => <li><h2><img src={i.icon} alt="multi" className="small" /> {i.title}</h2><p>{i.content}</p></li>)}
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}