﻿import React from "react";


export default class Contact extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: '' };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        alert('A name was submitted: ' + this.state.value);
        event.preventDefault();
    }

    render() {
        return (


            <div className="sectionroot mobile">
                <section>
                    <div className="sharedSection">
                        <div className="sectioncontent">
                            <div>
                                <h2>Contact us</h2>
                                <form onSubmit={this.handleSubmit}>
                                    <label>
                                        Name: <input type="text" value={this.state.value} onChange={this.handleChange} />
                                    </label>
                                    <input type="submit" value="Submit" />
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
            </div>




        );
    }
}


