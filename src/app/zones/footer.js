﻿import React, { Component } from 'react';


export default class Footer extends Component {

    render() {

        return(
            <footer>
                <section className="sharedSection">
                    <div className="sectioncontent">
                        <ul className="shared_grid">
                            <li>
                                <img src="img/user.white.png" alt="multi" className="mini" /> <h2>About</h2>
                                <p>The Swede(me), Rickard Magnusson a Senior fullstack developer, currently employeed as a consultant at Cap Gemini AS in Norway.</p>
                            </li>
                            <li>
                                <h2>Legal</h2>
                                <p>Everything on this site is just for my own education.</p>
                            </li>
                            <li>
                                <h2><img src="img/logo.svg" alt="Logo" className="small" /> Contact</h2>
                                <p>Email: bootproject@icloud.com</p>
                            </li>
                        </ul>
                        <div>(&copy;) Copyright www.bootproject.se 2018</div>
                    </div>
                   
                </section>
            </footer>
            );
    }

}