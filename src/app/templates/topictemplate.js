﻿import React, { Component } from 'react';
import Zone from "../components/zones";
import Nav from "../components/nav";
import Footer from "../zones/footer";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'; // ES6

export default class TopicTemplate extends Component {
    render() {
        return (

            <section>
                <div className="container">
                    <Nav title="Bootproject" />
                    <ReactCSSTransitionGroup
                        transitionName="example"
                        transitionAppear={true}
                        transitionAppearTimeout={300}
                        transitionEnter={false}
                        transitionLeave={false}>
                        <Zone zone="BeforeContent" module="Builder" component="Topic" />
                        <Zone zone="Content" />
                        <Zone zone="AfterContent" />
                        <Zone zone="AboutContent" />
                    </ReactCSSTransitionGroup>
                    <Footer />
                </div>
                </section>
               
        );
    }
}