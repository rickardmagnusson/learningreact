import React, { Component } from 'react';
import { Switch, BrowserRouter as Router, Route } from 'react-router-dom';
import { Data } from './api';

class App extends Component {

    render() {
        return (
            <Router>
                <Switch>
                    { Data.routes.map(r => <Route key={r.id} path={r.path} exact={r.exact} component={r.component} />)}
                </Switch>
            </Router>
        );
    }
}

export default App;