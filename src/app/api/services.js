
import Content from "../components/content";
import Projects from "../components/projects";
import Footer from "../components/footer";
import Topics from "../plugins/topics/topics";
import Template from "../templates/template";
import HomeTemplate from "../templates/hometemplate";
import TopicTemplate from "../templates/topictemplate";
import Triple from "../components/triple";
import Page from "../components/page";
import Post from "../plugins/topics/post";
import Contact from "../components/contact";

const Assets = {
    biglogo: {
        alt: "Bootproject logo",
        src: "img/biglogo.png"
    }

};

const Data = {
    topic: {
        title: "<span>Examples</span> <span>from</span> <span>our</span> <span>topics</span>",
        strategy: "/topics/"
    },
    topics: [
        {
            path: "my-topic",
            title: "My topics",
            content: "This is my first topic",
            component: Post
        },
        {
            path: "my-second-topic",
            title: "My second topic",
            content: "This is my second topic",
            component: Post
        },
        {
            path: "my-third-topic",
            title: "My third topic",
            content: "This is my third topic",
            component: Post
        }
    ], 
    projects: [
        {
            zone: "Content",
            html: "<h3>Senior fullstack developer</h3><p>Responsibilities: Developing and maintaining the companys sites in SharePoint. Focus on reducing deploymentstimes and increase the performance of applications. 24/7 responsebility for www.santanderconsumer.com</p>"
        },
        {
            zone: "Content",
            html: "<h3>Senior fullstack developer</h3><p>Responsibilities: Developing and maintaining the companys sites in SharePoint. Focus on reducing deploymentstimes and increase the performance of applications. 24/7 responsebility for www.santanderconsumer.com</p>"
        }
    ],
    content:[
        {
            zone: "Content",
            page: "/",
            html: "<section><div class=\"intro\"><div class=\"inner\"><h1><span>Boot</span><span>project</span></h1><h3>Current</h3><div class=\"area\"><img class=\"biglogo\" src=" + Assets.biglogo.src +" /></div></div></div></section>"
        }, 
        {
            zone: "Content",
            page: "/projects",
            html: "<h1>Projects</h1>"
        }
        ,
        {
            zone: "TripleFirst",
            page: "/",
            html: "<h4>Cap Gemini</h4><p>On monday I will start as a consultant developer on Cap Gemini. This is going to be exiting due to all new colueges and new projects.</p>"
        }
        ,
        {
            zone: "TripleSecond",
            page: "/",
            html: "<h4>Deconta(GmbH)</h4><p>Its been a pleasue to work with the guys from Deconta. Ill hope their find a new businesspartner in Sweden.</p>"
        }
        ,
        {
            zone: "TripleFirst",
            page: "/",
            html: "<h4>Multibutiken</h4><p>After a year at Multibutiken AB, I desided to move on. Its been an experience for me.</p>"
        },
        {
            zone: "TripleSecond",
            page: "/",
            html: "<h4>Santander Bank As</h4><p>This is by far the best workplaces ever. Its with a sad eye Ill move on.</p>"
        }
        ,
        {
            zone: "Content",
            page: "/contact",
            html: "<h1><span>Contact</span> <span>bootproject</span></h1><p>Feel free to contact. Use our contact form below.</p>"
        }

    ],
    routes: [
        {
            id: 1,
            title: "Home",
            path: "/",
            tmpl: HomeTemplate,
            component: Page,
            exact: true,
            zones: [
                {
                    name: "Content",
                    modules: [Content, Triple],
                    children: null
                }
                ,
                {
                    name: "Footer",
                    modules: [Footer],
                    children: null
                }
            ]
        },
        {
            id: 2,
            title: "Projects",
            path: "/projects",
            tmpl: HomeTemplate,
            component: Page,
            exact: false,
            zones: [
                {
                    name: "Content",
                    modules: [Content, Projects],
                    children: null
                }
                ,
                {
                    name: "Footer",
                    modules: [Footer],
                    children: null
                }
            ]
        },
        {
            id: 3,
            title: "Blog",
            path: "/topics",
            tmpl: HomeTemplate,
            component: HomeTemplate,
            exact: false,
            zones: [
                {
                    name: "ASideRight",
                    modules: [Topics],
                    children: "topics"
                },
                {
                    name: "Content",
                    modules: [Post],
                    children: null
                }
                ,
                {
                    name: "Footer",
                    modules: [Footer],
                    children: null
                }
            ]
        },
        {
            id: 1,
            title: "About",
            path: "#about",
            tmpl: HomeTemplate,
            component: Template,
            exact: true,
            zones: [
                {
                    name: "Content",
                    modules: [Content],
                    children: null
                }
                ,
                {
                    name: "Footer",
                    modules: [Footer],
                    children: null
                }
            ]
        },
         {
            id: 1,
            title: "Contact",
            path: "/contact",
            tmpl: HomeTemplate,
            component: Template,
            exact: true,
            zones: [
                {
                    name: "Content",
                    modules: [Content],
                    children: null
                }
                ,
                {
                    name: "Footer",
                    modules: [Footer],
                    children: null
                }
            ]
        }
    ]
};
export default Data;