﻿import HomeTemplate from "../templates/hometemplate";
import TopicTemplate from "../templates/topictemplate";
import Page from "../components/page";
import Contact from "../zones/contact";
import ContentItem from "../zones/contentitem";
import Timeline from "../zones/timeline";
import Builder from "../plugins/builder";
import Post from "../plugins/topics/post";



var TopicContent = {
    topic: {
        title: "Projects",
        strategy: "/projects/",
        path: "/projects"
    },
    topics: [
        {
            path: "multitenancy",
            title: "Multitenancy Core 2.2",
            content: "<br/><br/>Since its getting more common to use .Net Core projects, I needed to make the previous(4.6) Boot.Multitenancy project compatible with .Net Core. Boot.Multitenancy is a nHibernate extension that makes it very easy to use multiple domains and their databases. The new project is more lightweighted and is easy to install. Add two lines in Startup.cs, and your project is ready to server multiple sites. See the <u><a href='https://bitbucket.org/rickardmagnusson/bootproject.corev2/src/master/' class='underline' target='_blank'>project page</a></u> on bitbucket for more information.",
            component: Post
        },
        {
            path: "azure-durable-functions",
            title: "Deployment with durable functions (V1)/(V2)",
            content: "<br/><br/><img src='/img/azure.png' class='contentimg' alt='Flow, Azure' /><br/>In this project I needed to take on premise services to Azure. Like any other project it is just not that, its so much more. First of is the project it self, its made in .Net and not in Core wish makes it a bit harder since Azure durable function only exits in .NET Core. So after some research on how to achieve this, it failed because these services is connected to Dynamics CRM, and theres no support for Core in any way. (Not at this moment). Hmmm. How to proceed? I surely needed the durable function so I can control the services, so I decided to make the services call from Azure function V1, since they have support for .Net. So I created my V1 as .NET and the other V2 as a durable function, and the later one calls the V1 regular function with an async call. This is not an optimal solution, but still, it works.",
            component: Post
        },
        {
            path: "dynamics-365-on-docker",
            title: "Dynamics 365, Visual studio Code, docker deployment",
            content: "In a few weeks, I need to be prepared to develop in Dynamics 365. Can you actually learn Dynamics in such short time? Well I need to. Well not all, thats impossible. I need to know the basics. I'm start working with a new client soon where we are going to pull services and applications to Azure. I started search Internet for information about develop in Dynamics.<br/><br/> <h2>Installation</h2>First, I tried to install it on a VM in Azure, but there were so much configurations that I left that idea. There must be a easier solution to develop in Dynamics. And there was. I ended up installing the image in Docker. Also I fired up Visual studio Code, and installed the AL extension. The big disadvantage of the image is the size, 18GB. The benefit is that you will have a container that runs out of the box.<br/><br/>It's pretty easy to install the image. Install Docker, and sign up at Docker hub. Run the command (docker run -e accept_eula=Y -m 4G microsoft/bcsandbox)(4G is how much memory you will use). When the installation is finished you'll get all required parameters in the console. Username, password, url etc in cmd. And its ready to get fired up. Open the browser and install the .cer file(displayed in the cmd) and continue to Dynamics 365 Central, Neat Microsoft and Docker!!. Now your'e ready to deploy new code to Dynamics 365.<br/><br/><h2>AL (Application language)</h2>It seams sooo.. Microsoft to develop a new language. Reminds me of my time developing in Sharepoint. Anyhow, it seems quite straight forward, and to deploy to the container also worked as expected.<br/><br/><h2>Resources</h2><a class='active' href='https://hub.docker.com/r/microsoft/bcsandbox/' target='_blank'>Dynamics Docker 365 image</a>",
            component: Post
        },
        {
            path: "docker-kubernetes",
            title: "Azure AKS, Kubernetes deployment",
            content: "This week I've tested out the Azure AKS, Kubernetes (container) in Azure. But man, is that a lot of configurations to do!! I followed a blogpost (<a class='active' href='https://blogs.msdn.microsoft.com/azuredev/2018/03/27/building-microservices-with-aks-and-vsts-part-1' target='_blank'/>AKS VSTS</a> ) that a collegue of mine did, Andreas Helland, a blogpost at Microsoft on creating AKS in devOps(VSTS). As a former collegue of mine said about DevOps, - Is how to trick developers to work with deployment. So they tricked me as well, I guess LoL :-).<br/><br/>Im not kidding when I say that steps that has to be made.. Uhhh A lot!! Andreas addressed me on this...<br/><br/>Theres a lot of things you need to know before you get started. Azure CLI, Docker, Helm, Tiller, Hyper-V, Kubernetes, Kubernetes dashboard and ofcource Azure. I managed to create a lot of errors during the lesson, but as always, errors makes you stronger and better as a developer and also make you a better problem solver.. Aint right?<br/><br/> -Anyhow, I manage to get thrue all the steps and the only thing left is to make it public, with an A record in DNS. So I ended up to buying a new domain(amazingly available) www.footer.se. So hopefully you will see the project I made, end up in that domain. <br/><br/>We have to say thanks to Andreas Helland that made the blogpost!! Without your post, I would have been totally lost!!<br/><img src='/img/kubernetes-azure.png' class='contentimg' alt='Kubernetes, Azure(AKS)' /><br/><br/>",
            component: Post
        },
        {
            path: "cloud-flare-dns",
            title: "Cloudflare offers you free SSL and much more",
            content: "Since Google announced that the will exclude websites that doesnt use SSL, I investigated if there was a free service since I own a couple of domains. I found the Cloudflare service, with free SSL certificates. Cloudflare offers an easy way to manage your DNS thrue their controlpanel. This must be the website service of the year, the service is so good that even Alltin.no used it when they released the Skatteopp&#248;r(tax assessment) for the Norwegian peoples. You may wonder why, it's because of the cache service. You can actually turn off you site and just run it from their cache. Smooth isn't it. So head over to cloudflare and sign up for your free service. <a class='active' href='https://www.cloudflare.com/' target='_blank'>Cloudflare</a>  ",
            component: Post
        }
        ,
        {
            path: "azure-tables",
            title: "Azure tables, Storage",
            content: "Today ive tested out the Azure Table feature. In a simple, fast and secure way you can put content on the cloud and serve it as an API. Check it out!",
            component: Post
        },
        {
            path: "sanity",
            title: "Sanity react and GROQ",
            content: "When I started to run React applications, I run across Sanity.io. They offers a complete solution thrue Node. A frontend and a headless backend that is installed in minits. As they descibes their self - Build headless, collaborative editing environments in React.js. They also developed a query language, GROQ that is a bit similar to GraphQL. Ive tested out the applications and their query editor. They have an really good idea here, easy to install and extend thrue the scheme. Good work Sanity.",
            component: Post
        }
        ,
        {
            path: "creating-react-components",
            title: "Creating React Components",
            content: "This is all about create React JS components. The main thing with React is how easy it is to learn. If you understand the layout and continue making your own extensions, it will result in a application that is very fast to build. At least, thats my experience.",
            component: Post
        },
        {
            path: "project-module",
            title: "Project module",
            content: "A module that shows my current projects. This module is about learning subroutes, and how React are using Routes.",
            component: Post
        },
        {
            path: "next-up-blog",
            title: "Next up, Blog",
            content: "Next module to be created is a blog module. I will implement a Tag engine, where a Tag, or Tags is related to a Post in the blog.",
            component: Post
        }
    ]
};



var TimelineContent = {

    items: [
        {
            id: "1",
            path: "/",
            zone: "Timeline",
            style: "",
            title: "Bootproject timeline",
            content: "Latest projects and assaignments",
            list: [
                {
                    header: "Dynamics 365",
                    content: "Right now Im preparing for a new project for a bank institution. Its a new client that needs to move all projects from Dynamics On Premise to Azure."
                }
                ,
                {
                    header: "Kubernetes",
                    content: "KUBERNETES... Yummy. Can you taste the word? -Anyhow, my current goal is to create microservices with Kubernetes and to understand the Kubernetes and Docker pipeline."
                }
                ,
                {
                    header: "Azure tables, Storage",
                    content: "Build your API or content structure thrue Azure tables. An pretty easy way to put your api online in a secure way."
                }
                ,
                {
                    header: "Multitenancy",
                    content: ".NET package that makes ease of using multiple domains and databases. Download at NuGet. - install-package Boot.Multitenancy"
                },
                {
                    header: "Bitbucket",
                    content: "Bitbucket is a really nice platform to share your code, but also as for deployment to for eg. Azure, thrue their pipelines."
                },
                {
                    header: "React JS",
                    content: "React is a really great platform to make advanced content structures, in an easy way."
                }
                
            ]
        }
    ]
};



var ContentList = {
    items: [
        {
            id: "1",
            path: "/",
            zone: "Content",
            style: "",
            title: "Development base",
            content: "",
            icon: "/img/layers.svg",
            list: [
                {
                    icon: "img/layers.svg",
                    header: "Multitenancy",
                    content: ".NET package that makes ease of using multiple domains and databases. Download at NuGet"
                },
                {
                    icon: "img/bitbucket.png",
                    header: "Bitbucket",
                    content: "Bitbucket is a really nice platform to share your code, but also for deployment to for eg. Azure, thrue their piplines."
                },
                {
                    icon: "img/react.png",
                    header: "React JS",
                    content: "React is a really great platform to make advanced content structures, in an easy way."
                }
            ]
        },
        {
            id: "2",
            path: "/",
            zone: "Content",
            style: " mobile",
            title: "Render React in Zones",
            content: " Current project (Learing React) contains an implementation of creating Zones. A zone is a component that is defined in a template, then filled from a service API. When you define templates you just enter what zones to get React Components from. This website is built with the zone layout.Keeping it simple and generic is the way I like it.",
            icon: "/img/code.svg",
            list: []
        },
        {
            id: "3",
            path: "/",
            zone: "AfterContent",
            style: " mobile",
            title: "Project status",
            content: "This project moves into a complete application for websites/cellphones. Latest addition is the project module that renders content inside other contents.",
            icon: "/img/services.png",
            list: []
        },
        {
            id: "4",
            path: "/",
            zone: "AboutContent",
            style: "",
            title: "About",
            content: "Who? The nerd me(Rickard at Bootproject) currently employeed at Cap Gemini. A been in a lot of projects in the past. Latest project is about AKS, Azure Kubernetes container and how to deploy to it to a Azure container. Hopefully I will learn more, and also let other developers see my work thrue this application.",
            icon: "/img/user.png",
            list: []
        }
        ,
        {
            id: "5",
            path: "/projects",
            zone: "Content",
            style: "",
            title: "Projects",
            content: "Current project (Learing React) contains an implementation of creating Zones. A zone is a component that is defined in a template, then filled from a service API. When you define templates you just enter what zones to get React Components from. This website is built with the zone layout.Keeping it simple and generic is the way I like it.",
            icon: "/img/services.png",
            list: []
        }
        ,
        {
            id: "6",
            path: "/about",
            zone: "Content",
            style: "",
            title: "About",
            content: "Who? The nerd me(Rickard at Bootproject) currently employeed at Cap Gemini. A been in a lot of projects in the past. Latest project is about AKS, Azure Kubernetes container and how to deploy to it to a Azure container. Hopefully I will learn more, and also let other developers see my work thrue this application.",
            icon: "/img/services.png",
            list: []
        },
        {
            id: "7",
            path: "/blog",
            zone: "Content",
            style: "",
            title: "Yet to be developed",
            content: "Next module to be created is a blog. A was thinking about create a blog with links, -Tags and content dependent on that tags.",
            icon: "/img/services.png",
            list: []
        }
    ]
};


/** Grid(List) boxes below intro on homepage*/
var GridList = {
    items: [
        {
            id: "1",
            path: "/",
            title: "Development base",
            icon: "img/layers.svg",
            list: [
                {
                    icon: "/img/layers.svg",
                    header: "Multitenancy",
                    content: ".NET package that makes ease of using multiple domains and databases.Download at NuGet"
                },
                {
                    icon: "/img/bitbucket.png",
                    header: "Bitbucket",
                    content: "Bitbucket is a really nice platform to share your code, but also as for deployment to Azure, thrue their piplines."
                },
                {
                    icon: "/img/react.png",
                    header: "React JS",
                    content: "React is a really great platform to make dynamic html content structures, in an easy way."
                }
            ]
        }
    ]
};


/** Page Routes*/
var Data = {
    routes: [
        {
            id: 1,
            title: "Home",
            path: "/",
            tmpl: HomeTemplate,
            component: Page,
            exact: true,
            zones: [
                {
                    name: "Content",
                    modules: [ContentItem]
                }
                ,
                {
                    name: "Timeline",
                    modules: [Timeline]
                }
                ,
                {
                    name: "AfterContent",
                    modules: [ContentItem]
                },
                {
                    name: "AboutContent",
                    modules: [ContentItem]
                }
            ]
        },
        {
            id: 2,
            title: "Projects",
            path: "/projects",
            tmpl: TopicTemplate,
            component: TopicTemplate,
            exact: false,
            zones: [
                {
                    name: "BeforeContent",
                    modules: [Builder]
                }
               
            ]
        },
        {
            id: 3,
            title: "Blog",
            path: "/blog",
            tmpl: TopicTemplate,
            component: Page,
            exact: false,
            zones: [
                {
                    name: "Content",
                    modules: [ContentItem]
                }

            ]
        },
        {
            id: 4,
            title: "About",
            path: "/about",
            tmpl: TopicTemplate,
            component: Page,
            exact: false,
            zones: [
                {
                    name: "Content",
                    modules: [ContentItem]
                },
                {
                    name: "Content",
                    modules: [Contact]
                }

            ]
        }
    ]
};

export { Data, ContentList, GridList, TimelineContent, TopicContent };

