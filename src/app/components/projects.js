﻿import React, { Component } from 'react';
import Site from "../api/services";

import $ from "jquery";



$.fn.extend({
    timeline: function (options) {

        return this.each(function () {
            // Store content
            var $userContent = $(this).children('div');

            // Create required content divs
            $userContent.each(function () {
                $(this).addClass('timeline-content').wrap('<div class="timeline-point"><div class="timeline-block"></div></div>');
            });

            //Add icon
            $(this).find('.timeline-point').each(function () {
                $(this).prepend('<div class="timeline-icon"></div>');
            });
        });
    }
});




export default class Projects extends Component {

    constructor() {
        super();
        this.createMarkup = this.createMarkup.bind(this);
    }


    componentDidMount() {
        $(".timeliner").timeline();
    }


    createMarkup() {
        const contents = Site.projects.filter(c => c.zone === "Content");
        var html = "";

        if (contents !== undefined) {
            for (var c in contents) {
                html += "<div>" + contents[c].html + "</div>";
            }
        }
       
        return { __html: html };
    }

    render() {
        return (
            <div className="timeliner" dangerouslySetInnerHTML={this.createMarkup()} />
           
            );
            
        }
}