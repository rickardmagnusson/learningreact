import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Data } from "../api";

class Nav extends Component{

    constructor() {
        super();
        this.currentClass = this.currentClass.bind(this);
    }

    currentClass(path) {
        var url = document.location.pathname;
        var parts = url.split("/");
        if (url.split("/").length > 2)
        if (path === ("/" + parts[1]))
            return "nav-link text-dark active";

        return path === document.location.pathname ? "nav-link text-dark active" : "nav-link text-dark";
    }

    renderNav = () => {
        var routes = Data.routes;
        var links = [];
        for (var route in routes) {
            links.push(route);
  
            var zones = routes[route].zones;

            for (var zone in zones) {
                var tp = zones[zone].children;
                var childs = Data[tp];
                for (var c in childs) {
                    if (c !== undefined) {
                        links.push(c);
                    }
                } 
            }
        }
        return links;
    }

    renderClick(path) {
        try {
            var ga = window.ga;
            ga('set', 'page', path);
            ga('send', 'pageview');
        } catch (e) {
            console.log("Error: " + path);
        }
    }

	render(){
      

        return (

            <nav className="navbar navbar-expand-lg top">
                <a className="navbar-brand" href="/"><img src="img/logo.svg" alt="Logo" className="brand" /> {this.props.title}</a>
                <button className="navbar-toggler collapsed bg-light" type="button" data-toggle="collapse" data-target="#navbarsCollapse" aria-controls="navbarsCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span><i className="fa fa-bars fa-1x"/></span>
                </button>

                <div className="navbar-collapse collapse" id="navbarsCollapse">
                    <ul className="navbar-nav ml-auto">
                        {Data.routes.map(page =>
                            <li key={page.id}><Link className={this.currentClass(page.path)} to={page.path} key={page.path} onClick={() => this.renderClick(page.path)}>{page.title}</Link></li>
                         )}
                    </ul>
                </div>
            </nav>
        );
	}
}

export default Nav;