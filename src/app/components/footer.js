﻿import React, { Component } from 'react';
import ShowLocation from "../plugins/location/showlocation";

export default class Footer extends Component {
    render() {
        return (
            <footer>
                <div className="row">
                    <div className="col-md-12">
                        <ShowLocation />
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-4">
                        <h2>Contact</h2>
                        <p>Footer first</p>
                    </div>
                    <div className="col-md-4">
                        <h2>About</h2>
                        <p>About</p>
                    </div>
                    <div className="col-md-4">
                        <h2>Legal</h2>
                        <p>Legal information</p>
                    </div>
                </div>
             </footer>
        );
    }
}