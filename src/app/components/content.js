﻿import React, { Component } from 'react';
import Site from "../api/services";


export default class Content extends Component {

    constructor() {
        super();
        this.createMarkup = this.createMarkup.bind(this);
    }


    createMarkup() {
        const contents = Site.content.filter(c => c.zone === "Content" && c.page === document.location.pathname);
        var html = "";

        if (contents !== undefined) {
            for (var c in contents) {
                html += contents[c].html;
            }
        }

        return { __html: html};
    }

    render() {
        return (
            <div className="sectionroot">
                <section>
                    <div className="sharedSection">
                        <div className="sectioncontent">
                            <header><img src="layers.svg" alt="multi" className="mini" /> <h2>Development base</h2></header>
                            <ul className=" shared_grid">
                                <li>
                                    <h2><img src="layers.svg" alt="multi" className="small" /> Multitenancy</h2>
                                    <p>.NET package that makes ease of using multiple domains and databases. Download at NuGet</p>
                                </li>
                                <li>
                                    <h2><img src="bitbucket.png" className="small" /> Bitbucket</h2>
                                    <p>Bitbucket is a really nice platform to share your code, but also as for deployment to Azure, thrue their piplines.</p>
                                </li>
                                <li>
                                    <h2><img src="react.png" className="small" /> React JS</h2>
                                    <p>React is a really great platform to make advanced content structures, easy.</p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}