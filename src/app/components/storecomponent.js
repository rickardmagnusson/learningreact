﻿import React, { Component } from "react";

class StoreComponent  {

        componentDidMount() {
            this.checkLocalStorageExists();
        }

        checkLocalStorageExists() {
            const testKey = 'test';

            try {
                localStorage.setItem(testKey, testKey);
                localStorage.removeItem(testKey);
                this.setState({ localStorageAvailable: true });
            } catch (e) {
                this.setState({ localStorageAvailable: false });
            }
        }

        save(key, data) {
            if (this.state.localStorage)
                localStorage.setItem(key, data);
        }

        load(key) {
            if (this.state.localStorage)
                return localStorage.getItem(key);
        }

        remove(key) {
            if (this.state.localStorage)
                localStorage.removeItem(key);
        }

        render() {
            return (
                <React.Fragment>{
                    this.props.render({
                        load: this.load,
                        save: this.save,
                        remove: this.remove
                    })}
                </React.Fragment>
            );
        }

    }


export default StoreComponent;