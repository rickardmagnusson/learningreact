﻿import React, { Component } from 'react';
import Site from "../api/services";


export default class Triple extends Component {

    constructor() {
        super();
        this.createMarkup = this.createMarkup.bind(this);
    }


    createMarkup(zone) {
        const contents = Site.content.filter(c => c.zone === zone && c.page === document.location.pathname);
        var html = "";

        if (contents !== undefined) {
            for (var c in contents) {
                html += contents[c].html;
            }
        }

        return { __html: html };
    }

    render() {
        return (
            <div className="row">
                <div className="col-md-6" dangerouslySetInnerHTML={this.createMarkup("TripleFirst")} />
                <div className="col-md-6" dangerouslySetInnerHTML={this.createMarkup("TripleSecond")} />
            </div>
            );
    }
}