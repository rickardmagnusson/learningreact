import React, { Component } from 'react';

import { Data } from '../api';


/**
 * Base page of all routes.
 * Templates from configuration.
 */
class Page extends Component {


    constructor(props) {

        super(props);

        this.state = {
            tmpl: [],
            currentpath: document.location.pathname
        };

        this.renderElement = this.renderElement.bind(this);
        this.recurseElements = this.recurseElements.bind(this);
    }



    /**
    * Add properties.
    */
    componentDidMount() {

        var th = this;
        const routes = Data.routes.map(r => r);

        /** Filter routes to get current.*/
        const page = routes.filter(r => r.path === this.state.currentpath);

        th.setState({
            tmpl: page[0].tmpl
        });
    }




     /**
     * Renders a React Component
     * @param {Object} component React.Component
     * @returns {Object} React component.
     */
    renderElement(component) {

        var rand = "comp" + Math.random(1000).toString(); //Theres no better way to set dynamic id for a generic React component..
        
        return React.createElement(component, {
            data: this.props,
            key: rand
        });
    }




     /**
     * Enumerates an array of generic components
     * @param {Array} element The item to iterate or create
     * @returns {Object} Generally a react component.
     */
    recurseElements(element) {
        return Object.prototype.toString.call(element) === '[object Array]'
            ? element.map(this.recurseElements) : this.renderElement(element);
    }




    render() {

        /** Render elements (Zones) in template*/
        var elements = this.recurseElements(this.state.tmpl);
        return React.createElement("div", { key: "content", data: this.state }, elements);
    }
}

export default Page;