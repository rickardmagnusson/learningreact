import React, { Component } from 'react';
import { Data } from '../api';


/**
 * Renders a generic Components from the current url and requested Zone.
 */
class Zone extends Component{

    constructor(props) {
        super(props);
        this.state = {};

        /*  Function bindings */
        this.renderElement = this.renderElement.bind(this);
        this.recurseElements = this.recurseElements.bind(this);
        this.createZoneElements = this.createZoneElements.bind(this);
        this.getProps = this.getProps.bind(this);

        /** Current url */
        this.currentpath = document.location.pathname;
    }



     /**
     * Zones placeholer.
     * @returns {Array} zones
     */
    getInitialState() {
        return {
            zones: []
        };
    }



     /**
     * Add properties.
     */
    componentDidMount() {

        var th = this;

        /** Routes from service api.*/
        const routes = Data.routes.map(r => r);
       
        /** Filter routes to get current.*/
        const page = routes.filter(r => r.path === this.currentpath);

        /** Get all zones from current route.*/
        this.zones = page.map(r => r.zones);
        
        th.setState({
            zones: page.map(r => r.zones)
        });

    }




    getProps(data) {
        try {
            const newData = Object.keys(data).reduce((result, currentKey) => {
                if (typeof data[currentKey] === 'string' || data[currentKey] instanceof String) {
                    //console.log(currentKey);
                    result.push(currentKey);
                } else {
                    const nested = this.getProps(data[currentKey]);
                    //console.log(...nested);
                    result.push(...nested);
                }
                return result;
            }, []);
            return newData;
        } catch (e) {
            //console.log(e);
            return;
        }
    }


     /**
     * Renders a React Component
     * @param {Object} component React.Component
     * @param {string} zone A zone element
     * @returns {Object} React component.
     */
    renderElement(component, zone) {

        var rand = "comp" + Math.random(1000).toString(); 
        return React.createElement(component, {
            key: rand,
            props: { zone: zone, Component: component }
        });
    }



     /**
     * Enumerates an array of generic components
     * @param {Array} item The item to iterate or create
     * @param {string} zone A zone element
     * @returns {Object} Generally a react component.
     */
    recurseElements(item, zone) {
        return Object.prototype.toString.call(item) === '[object Array]'
            ? item.map(this.recurseElements) : this.renderElement(item, zone);
    }




     /**
     * Pushes the elements to an array of components.
     * @param {Array} zones Array of zones.
     * @param {string} zone A zone element
     * @returns {Array} An array of components for the zone.
     */
    createZoneElements(zones, zone) {

        var elements = [];
        //Get attributes from component.
        for (var item in zones) {
            for (var i = 0; i < zones[item].length; i++) {
                if (zones[item][i].name === this.props.zone) {
                    const modules = zones[item][i].modules.map(m => m);
                    for (var m in modules) {
                        elements.push(this.recurseElements(modules[m], zones[item][i].name));
                    }
                }
            }
        }
        return elements;
    }



    render() {

        this.getProps(this.props.data);
        //console.log(this.props.data);

        /** Render zones*/
        var elements = this.createZoneElements(this.state.zones);

        return React.createElement("div", { key: "content", props: this.props }, elements);
    }
}

export default Zone;